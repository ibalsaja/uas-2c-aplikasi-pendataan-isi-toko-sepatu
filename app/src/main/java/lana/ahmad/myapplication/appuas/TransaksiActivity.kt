package lana.ahmad.myapplication.appuas

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_transaksi.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.reflect.Method
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class TransaksiActivity: AppCompatActivity(), View.OnClickListener {
    val activity = this@TransaksiActivity
    lateinit var mediaHelper : MediaHelper
    lateinit var trsAdapter : AdapterDataTrs
    lateinit var jenisAdapter : ArrayAdapter<String>
    lateinit var ukuranAdapter : ArrayAdapter<String>
    var daftarTrs = mutableListOf<HashMap<String,String>>()
    var daftarJenis = mutableListOf<String>()
    var daftarUkuran = mutableListOf<String>()
    val url = "http://192.168.1.11/uas/show_data.php"
    val url2 = "http://192.168.1.11/uas/get_ukuran.php"
    val url3 = "http://192.168.1.11/uas/get_jenis.php"
    val  url4 = "http://192.168.1.11/uas/query_ins_del_upd.php.php"
    var imStr = ""
    var pilihUkuran = ""
    var pilihJenis = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaksi)
        trsAdapter = AdapterDataTrs(daftarTrs,this)
        mediaHelper = MediaHelper(this)
        lsSptu.layoutManager = LinearLayoutManager(this)
        lsSptu.adapter = trsAdapter
        jenisAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarJenis)
        spJenis.adapter = jenisAdapter
        spJenis.onItemSelectedListener = itemSelected1
        ukuranAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarUkuran)
        spSize.adapter = ukuranAdapter
        spSize.onItemSelectedListener = itemSelected2
        imgUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnTambah.setOnClickListener(this)
        btnDelete.setOnClickListener(this)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_setting,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSet->{
                val intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        //showDataTrs("")
        //getJenis()
        //getUkuran()
    }

    val itemSelected1 = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spJenis.setSelection(0)
            pilihJenis = daftarJenis.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihJenis = daftarJenis.get(position)
        }
    }
    val itemSelected2 = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spSize.setSelection(0)
            pilihUkuran = daftarUkuran.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihUkuran = daftarUkuran.get(position)
        }
    }


    fun showDataTrs(namaTrs:String){
        val request =object : StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarTrs.clear()
                Toast.makeText(this,"koneksi ke server (sepatu)",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var Trs = HashMap<String,String>()
                    Trs.put("merk",jsonObject.getString("merk"))
                    Trs.put("warna",jsonObject.getString("warna"))
                    Trs.put("jenis",jsonObject.getString("jenis"))
                    Trs.put("ukuran",jsonObject.getString("ukuran"))
                    Trs.put("url",jsonObject.getString("url"))
                    daftarTrs.add(Trs)
                }
                trsAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("merk",namaTrs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getUkuran(){
        val request = StringRequest(Request.Method.POST,url2,
            Response.Listener { response ->
                daftarUkuran.clear()
                Toast.makeText(this,"koneksi ke server (ukuran)",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarUkuran.add(jsonObject.getString("ukuran"))
                }
                ukuranAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun getJenis(){
        val request = StringRequest(Request.Method.POST,url3,
            Response.Listener { response ->
                daftarJenis.clear()
                Toast.makeText(this,"koneksi ke server (jenis)",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarJenis.add(jsonObject.getString("jenis"))
                }
                jenisAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload->{
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnTambah->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete->{
                queryInsertUpdateDelete("delete")
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode==Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,imgUpload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url4,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error= jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_SHORT).show()
                    showDataTrs("")
                }
                else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung dengan server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("merk",edMerk.text.toString())
                        hm.put("warna",edWarna.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",nmFile)
                        hm.put("jenis",pilihJenis)
                        hm.put("ukuran",pilihUkuran)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("merk",edMerk.text.toString())
                        hm.put("warna",edWarna.text.toString())
                        hm.put("images",imStr)
                        hm.put("file",nmFile)
                        hm.put("jenis",pilihJenis)
                        hm.put("ukuran",pilihUkuran)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("merk",edMerk.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
