package lana.ahmad.myapplication.appuas

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_transaksi.*

class AdapterDataTrs (val dataTrs : List<HashMap<String,String>>, val TransaksiActivity : TransaksiActivity) : RecyclerView.Adapter<AdapterDataTrs.HolderDataTransaksi>(){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataTrs.HolderDataTransaksi {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_transaksi,parent,false)
        return HolderDataTransaksi(v)
    }

    override fun getItemCount(): Int {
        return dataTrs.size
    }

    override fun onBindViewHolder(holder: AdapterDataTrs.HolderDataTransaksi, position: Int) {
        val data = dataTrs.get(position)
        holder.txMerk.setText(data.get("merk"))
        holder.txTipe.setText(data.get("tipe"))
        holder.txWarna.setText(data.get("warna"))
        holder.txUkuran.setText(data.get("ukuran"))
        if(position.rem(2)==0){
            holder.CLayout.setBackgroundColor(Color.rgb(230,245,240))
        }
        else{
            holder.CLayout.setBackgroundColor(Color.rgb(255,255,245))
        }
        holder.CLayout.setOnClickListener(View.OnClickListener {
            TransaksiActivity.edMerk.setText(data.get("merk"))
        })
    }

    inner class HolderDataTransaksi(v: View): RecyclerView.ViewHolder(v){
        val txMerk = v.findViewById<TextView>(R.id.txMerk)
        val txTipe = v.findViewById<TextView>(R.id.txTipe)
        val txWarna = v.findViewById<TextView>(R.id.txWarna)
        val txUkuran = v.findViewById<TextView>(R.id.txUkuran)
        val CLayout = v.findViewById<ConstraintLayout>(R.id.CLayout)
    }
}