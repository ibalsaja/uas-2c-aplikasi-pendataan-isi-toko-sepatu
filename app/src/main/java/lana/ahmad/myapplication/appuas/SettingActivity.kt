package lana.ahmad.myapplication.appuas

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.activity_transaksi.*
import kotlinx.android.synthetic.main.row_transaksi.*
import lana.ahmad.myapplication.appuas.R.layout.activity_setting

class SettingActivity : AppCompatActivity(){

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FIELD_FONT_SIZE = "font_size"
    val DEF_FONT_SIZE = 12

//    val onSeek = object : SeekBar.OnSeekBarChangeListener{
//        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
//            txMerk.setTextSize(progress.toFloat())
//            txTipe.setTextSize(progress.toFloat())
//            txWarna.setTextSize(progress.toFloat())
//            txUkuran.setTextSize(progress.toFloat())
//        }
//
//        override fun onStartTrackingTouch(p0: SeekBar?) {
//            TODO("Not yet implemented")
//        }
//
//        override fun onStopTrackingTouch(p0: SeekBar?) {
//            TODO("Not yet implemented")
//        }
//    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.row_transaksi)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
//        txMerk.setTextSize(preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat())
//        txTipe.setTextSize(preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat())
//        txWarna.setTextSize(preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat())
//        txUkuran.setTextSize(preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat())
        Toast.makeText(this,"Perubahan telah disimpan.",Toast.LENGTH_SHORT).show()


    }

}