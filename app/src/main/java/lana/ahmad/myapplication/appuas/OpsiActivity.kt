package lana.ahmad.myapplication.appuas

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class OpsiActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FIELD_FONT_SIZE = "font_size"
    val DEF_FONT_SIZE = 10

//    val onSeek = object : SeekBar.OnSeekBarChangeListener {
//        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
//            edMerk.setTextSize(progress.toFloat())
//            edTipe.setTextSize(progress.toFloat())
//            edWarna.setTextSize(progress.toFloat())
//            edUkuran.setTextSize(progress.toFloat())
//        }
//
//        override fun onStartTrackingTouch(p0: SeekBar?) {
//            TODO("Not yet implemented")
//        }
//
//        override fun onStopTrackingTouch(p0: SeekBar?) {
//            TODO("Not yet implemented")
//        }
//    }


    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putInt(FIELD_FONT_SIZE,sbar.progress)
        prefEditor.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
//        edMerk.textSize = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat()
//        edTipe.textSize = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat()
//        edWarna.textSize = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat()
//        edUkuran.textSize = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE).toFloat()
        sbar.progress = preferences.getInt(FIELD_FONT_SIZE,DEF_FONT_SIZE)
        btnSimpan.setOnClickListener(this)
    }
}